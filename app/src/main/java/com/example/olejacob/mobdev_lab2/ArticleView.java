package com.example.olejacob.mobdev_lab2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ArticleView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_view);

        WebView articleView = findViewById(R.id.articleView);
        String url = getIntent().getStringExtra("url");

        articleView.setWebViewClient(new WebViewClient());
        articleView.loadUrl(url);
    }
}
