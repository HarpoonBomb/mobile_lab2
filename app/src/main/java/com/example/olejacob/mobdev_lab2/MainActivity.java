package com.example.olejacob.mobdev_lab2;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView feedList;
    String url;
    String feedLimitString;
    Integer feedLimit;
    String refreshRateString;
    Integer refreshRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity);

        //Load preferences
        SharedPreferences urlPreferences = this.getSharedPreferences("urlPreferences", MODE_PRIVATE);
        url = urlPreferences.getString("url", "");
        SharedPreferences feedLimitPreferences = this.getSharedPreferences("feedLimitPreferences", MODE_PRIVATE);
        feedLimitString = feedLimitPreferences.getString("feedLimitSelected", "10");
        SharedPreferences refreshRatePreferences = this.getSharedPreferences("refreshRatePreferences", MODE_PRIVATE);
        refreshRateString = refreshRatePreferences.getString("refreshRateSelected", "10 minutes");
        updateFLRR();

        //User preferences
        findViewById(R.id.userPreferences).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MainActivity.this, UserPreferences.class), 1);
            }
        });

        //Update feed list
        feedList = findViewById(R.id.feedList);
        feedList.setLayoutManager(new LinearLayoutManager(this));
        feedList.setAdapter(new RssFeedAdapter(null));
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    updateFeedList();
                    try {
                        Thread.sleep(refreshRate * 60000);
                    } catch (InterruptedException error) {
                        Log.e("InterruptedException", "Error: " + error);
                    }
                }
            }
        }).start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        url = data.getStringExtra("url");
        feedLimitString = data.getStringExtra("feedLimit");
        refreshRateString = data.getStringExtra("refreshRate");
        updateFLRR();
        new Thread(new Runnable() {
            @Override
            public void run() {
                updateFeedList();
            }
        }).start();
    }

    public void updateFLRR() {
        feedLimit = Integer.parseInt(feedLimitString);
        if (refreshRateString == "Hourly") {
            refreshRate = 60;
        } else if (refreshRateString == "Daily") {
            refreshRate = 60 * 24;
        } else {
            refreshRate = 10;
        }
    }

    public void updateFeedList() {
        if (url != "") {
            try {
                URL rssURL = new URL(url);
                InputStream inputStream = rssURL.openConnection().getInputStream();
                XmlPullParser xmlPullParser = Xml.newPullParser();
                xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                xmlPullParser.setInput(inputStream, null);
                xmlPullParser.nextTag();

                Integer i = 0;
                //Boolean isItem = false;
                List<RssItem> items = new ArrayList<>();
                String title = null;
                String link = null;
                while (xmlPullParser.next() != xmlPullParser.END_DOCUMENT && i < feedLimit) {
                    String name = xmlPullParser.getName();
                    if (name != null) {
                        String content = "";
                        if (xmlPullParser.next() == xmlPullParser.TEXT){
                            content = xmlPullParser.getText();
                            xmlPullParser.nextTag();
                        }
                        if (!content.trim().isEmpty()) {
                            if (name.equalsIgnoreCase("title")) {
                                title = content;
                            } else if (name.equalsIgnoreCase("link")) {
                                link = content;
                            }

                            if (title != null && link != null) {
                                RssItem item = new RssItem(title, link);
                                items.add(item);
                                i++;
                                title = null;
                                link = null;
                            }
                        }
                    }
                }
                inputStream.close();
                final List<RssItem> finalItems = items;
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        feedList.setAdapter(new RssFeedAdapter(finalItems));
                    }
                });
            } catch (MalformedURLException error) {
                Log.e("MalformedURLException", "Error: ", error);
            } catch (IOException error) {
                Log.e("IOException", "Error: ", error);
            } catch (XmlPullParserException error) {
                Log.e("XmlPullParserException", "Error: ", error);
            }
        }
    }

    public class RssItem {
        public String title;
        public String link;

        public RssItem(String title, String link) {
            this.title = title;
            this.link = link;
        }
    }

    public class RssFeedAdapter extends RecyclerView.Adapter<RssFeedAdapter.RssFeedViewHolder> {
        private List<RssItem> rssItems;

        public class RssFeedViewHolder extends RecyclerView.ViewHolder {
            private View rssFeedView;

            public RssFeedViewHolder(View view) {
                super(view);
                rssFeedView = view;
            }
        }

        public RssFeedAdapter(List<RssItem> _rssItems) {
            rssItems = _rssItems;
        }

        @Override
        public RssFeedViewHolder onCreateViewHolder(ViewGroup parent, int type) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rss_feed_layout, parent, false);
            RssFeedViewHolder holder = new RssFeedViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(RssFeedViewHolder holder, int position) {
            final RssItem rssItem = rssItems.get(position);
            ((TextView)holder.rssFeedView.findViewById(R.id.title)).setText(rssItem.title);
            ((TextView)holder.rssFeedView.findViewById(R.id.link)).setText(rssItem.link);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("Link", rssItem.link);
                    Intent intent = new Intent(MainActivity.this, ArticleView.class);
                    intent.putExtra("url", rssItem.link);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            if (rssItems != null){
                return rssItems.size();
            } else {
                return 0;
            }
        }
    }
}
