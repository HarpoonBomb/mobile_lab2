package com.example.olejacob.mobdev_lab2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class UserPreferences extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_preferences);

        //Feed limit
        Spinner feedLimit = findViewById(R.id.feedLimitSpinner);
        String[] feedLimitList = new String[]{"10", "20", "50", "100"};
        ArrayAdapter<String> feedLimitAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, feedLimitList);
        feedLimit.setAdapter(feedLimitAdapter);

        //Refresh rate
        Spinner refreshRate = findViewById(R.id.refreshFrequencySpinner);
        String[] refreshRateList = new String[]{"10 minutes", "Hourly", "Daily"};
        ArrayAdapter<String> refreshRateAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, refreshRateList);
        refreshRate.setAdapter(refreshRateAdapter);

        //Load preferences
        SharedPreferences urlPreferences = this.getSharedPreferences("urlPreferences", MODE_PRIVATE);
        String urlString = urlPreferences.getString("url", "");
        EditText url = findViewById(R.id.URL);
        url.setText(urlString);
        SharedPreferences feedLimitPreferences = this.getSharedPreferences("feedLimitPreferences", MODE_PRIVATE);
        String feedLimitValue = feedLimitPreferences.getString("feedLimitSelected", "");
        feedLimit.setSelection(feedLimitAdapter.getPosition(feedLimitValue));
        SharedPreferences refreshRatePreferences = this.getSharedPreferences("refreshRatePreferences", MODE_PRIVATE);
        String refreshRateValue = refreshRatePreferences.getString("refreshRateSelected", "");
        refreshRate.setSelection(refreshRateAdapter.getPosition(refreshRateValue));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();

        EditText url = findViewById(R.id.URL);
        String urlText = url.getText().toString();
        SharedPreferences urlPreferences = this.getSharedPreferences("urlPreferences", MODE_PRIVATE);
        SharedPreferences.Editor urlEditor = urlPreferences.edit();
        urlEditor.putString("url", urlText);
        urlEditor.apply();

        //Feed limit save
        Spinner feedLimit = findViewById(R.id.feedLimitSpinner);
        SharedPreferences feedLimitPreferences = this.getSharedPreferences("feedLimitPreferences", MODE_PRIVATE);
        SharedPreferences.Editor feedLimitEditor = feedLimitPreferences.edit();
        feedLimitEditor.putString("feedLimitSelected", feedLimit.getSelectedItem().toString());
        feedLimitEditor.apply();

        //Refresh rate save
        Spinner refreshRate = findViewById(R.id.refreshFrequencySpinner);
        SharedPreferences refreshRatePreferences = this.getSharedPreferences("refreshRatePreferences", MODE_PRIVATE);
        SharedPreferences.Editor refreshRateEditor = refreshRatePreferences.edit();
        refreshRateEditor.putString("refreshRateSelected", refreshRate.getSelectedItem().toString());
        refreshRateEditor.apply();

        //Send preferences to main
        intent.putExtra("url", urlText);
        intent.putExtra("feedLimit", feedLimit.getSelectedItem().toString());
        intent.putExtra("refreshRate", refreshRate.getSelectedItem().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}
